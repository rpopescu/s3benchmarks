# S3 Performance Benchmarks

## Usage
```
$ go run main.go
  -b, --bucket string      S3 bucket
  -e, --endpoint string    S3 endpoint
  -E, --extended-output    Output extended results, including invididual request timing
  -f, --force-path-style   force path-style DNS buckets (i.e. endpoint/bucket/filename)
  -t, --num-threads int    number of threads (default 1)
  -n, --num-uploads int    number of uploads (default 1000)
  -o, --output string      output file for the results
  -s, --payload-size int   size of payload (default 4096)
  -T, --truncate-output    truncate the output file
```

## License and copyright

See LICENSE in the project root.
