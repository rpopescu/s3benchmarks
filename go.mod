module github.com/cvmfs/s3benchmarks

go 1.12

require (
	github.com/aws/aws-sdk-go v1.18.6
	github.com/spf13/pflag v1.0.3
	golang.org/x/net v0.0.0-20190320064053-1272bf9dcd53 // indirect
)
