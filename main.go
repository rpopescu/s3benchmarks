package main

import (
	"bytes"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"io"
	"math/rand"
	"os"
	"sync"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/spf13/pflag"
)

const (
	reqHead = iota
	reqHeadNew
	reqPut
	reqPutNew
)

var bucket string
var disableSSL bool
var endpoint string
var extendedOutput bool
var forcePathStyle bool
var numThreads int
var outputFile string
var payloadSize int // size of payload in bytes
var numUploads int  // number of uploads
var truncateOutput bool

// Result contains the type and the duration of a request
type Result struct {
	Type     int           // reqHead, reqHeadNew, putReq, putReqNew
	Duration time.Duration // duration of request in seconds
	Success  bool
}

// Payload is a randomly generate buffer for uploading
type Payload struct {
	Key  string
	Data []byte
}

func makePayload(size int) (*Payload, error) {
	p := make([]byte, size)
	rand.Read(p)
	h := sha1.New()
	if _, err := io.Copy(h, bytes.NewReader(p)); err != nil {
		return nil, fmt.Errorf("Could not compute SHA1 of payload")
	}
	key := hex.EncodeToString(h.Sum(nil))
	return &Payload{Key: key, Data: p}, nil
}

func headRequest(cl *s3.S3, bucket *string, key *string) Result {
	t0 := time.Now()
	result := Result{Type: reqHead, Success: true}
	input := s3.HeadObjectInput{
		Bucket: bucket,
		Key:    key,
	}
	_, err := cl.HeadObject(&input)
	if err != nil {
		if awsError, ok := err.(awserr.Error); ok && awsError.Code() == "NotFound" {
			result.Type = reqHeadNew
		} else {
			result.Success = false
		}
	}
	result.Duration = time.Since(t0)
	return result
}

func putRequest(cl *s3.S3, bucket *string, payload *Payload, exists bool) Result {
	t0 := time.Now()
	reqType := reqPutNew
	if exists {
		reqType = reqPut
	}
	result := Result{Type: reqType, Success: true}
	input := s3.PutObjectInput{
		Bucket: bucket,
		Key:    &payload.Key,
		Body:   bytes.NewReader(payload.Data),
	}
	_, err := cl.PutObject(&input)
	if err != nil {
		result.Success = false
	}
	result.Duration = time.Since(t0)
	return result
}

func main() {
	// Commandline flags
	pflag.StringVarP(&bucket, "bucket", "b", "", "S3 bucket")
	pflag.StringVarP(&endpoint, "endpoint", "e", "", "S3 endpoint")
	pflag.BoolVarP(&extendedOutput, "extended-output", "E", false, "Output extended results, including invididual request timing")
	pflag.BoolVarP(&forcePathStyle, "force-path-style", "f", false, "force path-style DNS buckets (i.e. endpoint/bucket/filename)")
	pflag.IntVarP(&numThreads, "num-threads", "t", 1, "number of threads")
	pflag.StringVarP(&outputFile, "output", "o", "", "output file for the results")
	pflag.IntVarP(&payloadSize, "payload-size", "s", 4096, "size of payload")
	pflag.IntVarP(&numUploads, "num-uploads", "n", 1000, "number of uploads")
	pflag.BoolVarP(&truncateOutput, "truncate-output", "T", false, "truncate the output file")
	pflag.Parse()

	// AWS setup
	awsCreds := credentials.NewEnvCredentials()
	awsCfg := aws.NewConfig().
		WithCredentials(awsCreds).
		WithRegion("cern").
		WithEndpoint(endpoint).
		WithS3ForcePathStyle(forcePathStyle).
		WithDisableSSL(false)

	sesh, err := session.NewSession(awsCfg)
	if err != nil {
		fmt.Printf("could not initialize S3 session: %v\n", err)
		os.Exit(1)
	}

	// Seed default random source
	rand.Seed(time.Now().UnixNano())

	// create channel to collect outputs, with a large enough buffer for all the requests
	totalRequests := numUploads * 4
	output := make(chan Result, totalRequests)

	// Barriers for timing
	start := sync.WaitGroup{}
	start.Add(numThreads)
	payloadsGenerated := sync.WaitGroup{}
	payloadsGenerated.Add(numThreads)
	headRequests1Done := sync.WaitGroup{}
	headRequests1Done.Add(numThreads)
	putRequests1Done := sync.WaitGroup{}
	putRequests1Done.Add(numThreads)
	headRequests2Done := sync.WaitGroup{}
	headRequests2Done.Add(numThreads)
	putRequests2Done := sync.WaitGroup{}
	putRequests2Done.Add(numThreads)

	for i := 0; i < numThreads; i++ {
		threadID := i
		go func() {
			start.Done()
			// Compute the work chunk size for the current threads
			chunk := getChunk(numUploads, numThreads, threadID)

			// Create payloads in advance
			payloads := make([]Payload, chunk)
			for j := 0; j < chunk; j++ {
				p, err := makePayload(payloadSize)
				if err != nil {
					fmt.Printf("could not generate payload: %v\n", err)
					os.Exit(1)
				}
				payloads[j] = *p
			}
			payloadsGenerated.Done()

			s3cl := s3.New(sesh)

			// Make a set of HEAD requests which should return NotFound
			for j := 0; j < chunk; j++ {
				output <- headRequest(s3cl, &bucket, &payloads[j].Key)
			}
			headRequests1Done.Done()

			// Make a set of PUT requests
			for j := 0; j < chunk; j++ {
				output <- putRequest(s3cl, &bucket, &payloads[j], false)
			}
			putRequests1Done.Done()

			// Re-make the set of HEAD requests which now should return OK
			for j := 0; j < chunk; j++ {
				output <- headRequest(s3cl, &bucket, &payloads[j].Key)
			}
			headRequests2Done.Done()

			// Re-make the set of PUT requests (overwrite)
			for j := 0; j < chunk; j++ {
				output <- putRequest(s3cl, &bucket, &payloads[j], true)
			}
			putRequests2Done.Done()
		}()
	}

	var times [6]time.Time
	fmt.Print("Starting benchmark loop")
	start.Wait()
	times[0] = time.Now()
	payloadsGenerated.Wait()
	times[1] = time.Now()
	fmt.Print("Payload generation complete")
	headRequests1Done.Wait()
	times[2] = time.Now()
	fmt.Print("HEAD (NotFound) requests complete")
	putRequests1Done.Wait()
	times[3] = time.Now()
	fmt.Print("PUT (new) requests complete")
	headRequests2Done.Wait()
	times[4] = time.Now()
	fmt.Print("HEAD (Found) requests complete")
	putRequests2Done.Wait()
	times[5] = time.Now()
	fmt.Print("PUT (existing) requests complete")

	results := make([]Result, 0, totalRequests)
	finished := 0
	for finished < totalRequests {
		results = append(results, <-output)
		finished++
	}

	var sink io.Writer = os.Stdout
	if outputFile != "" {
		flags := os.O_WRONLY | os.O_CREATE
		if truncateOutput {
			flags |= os.O_TRUNC
		} else {
			flags |= os.O_APPEND
		}
		f, err := os.OpenFile(outputFile, flags, 0644)
		if err != nil {
			fmt.Printf("could not open output file for writing\n")
			os.Exit(1)
		}
		sink = f
		defer f.Close()
	}
	printResults(sink, results, times)
}

func getChunk(numElem, numThreads, id int) int {
	rem := numElem % numThreads
	div := numElem / numThreads
	x := 0
	if id < rem {
		x = 1
	}
	return div + x
}

func printResults(out io.Writer, results []Result, times [6]time.Time) {
	n := float64(numUploads)
	fmt.Fprintf(out, "%v\n", time.Now())
	fmt.Fprintf(out, "  - Endpoint: %v\n", endpoint)
	fmt.Fprintf(out, "  - Bucket: %v\n", bucket)
	fmt.Fprintf(out, "  - Number of threads: %v\n", numThreads)
	fmt.Fprintf(out, "  - Number of uploads: %v\n", numUploads)
	fmt.Fprintf(out, "  - Payload size: %v\n", payloadSize)
	fmt.Fprintf(out,
		"  - Payload generation time (seconds): %v\n",
		times[1].Sub(times[0]).Seconds())

	var dts [4]float64
	var rates [4]float64
	for i := 0; i < 4; i++ {
		dts[i] = times[i+2].Sub(times[i+1]).Seconds()
		rates[i] = n / dts[i]
	}
	throughput := []float64{
		rates[1] * float64(payloadSize) / 1024.0 / 1024.0,
		rates[3] * float64(payloadSize) / 1024.0 / 1024.0}

	fmt.Fprintf(out, "Time for %v requests (s):\n", numUploads)
	fmt.Fprintf(out, "  - HEAD (NotFound): %v\n", dts[0])
	fmt.Fprintf(out, "  - PUT (new): %v\n", dts[1])
	fmt.Fprintf(out, "  - HEAD (Found): %v\n", dts[2])
	fmt.Fprintf(out, "  - PUT (existing): %v\n", dts[3])
	fmt.Fprintf(out, "Request rates (reqs/s):\n")
	fmt.Fprintf(out, "  - HEAD (NotFound): %v\n", rates[0])
	fmt.Fprintf(out, "  - PUT (new): %v\n", rates[1])
	fmt.Fprintf(out, "  - HEAD (Found): %v\n", rates[2])
	fmt.Fprintf(out, "  - PUT (existing): %v\n", rates[3])
	fmt.Fprintf(out, "Average throughput for PUT (MB/s):\n")
	fmt.Fprintf(out, "  - PUT (new): %v\n", throughput[0])
	fmt.Fprintf(out, "  - PUT (existing): %v\n", throughput[1])

	if extendedOutput {
		fmt.Fprintf(out, "Individual request times\n")
		fmt.Fprintf(out, "Request type, Duration, Success\n")
		for _, v := range results {
			fmt.Fprintf(out, "%v,%v,%v\n", printReqType(v.Type), v.Duration.Seconds(), v.Success)
		}
	}
}

func printReqType(t int) string {
	d := ""
	switch t {
	case reqHead:
		d = "HEAD"
	case reqHeadNew:
		d = "HEAD (new)"
	case reqPut:
		d = "PUT"
	case reqPutNew:
		d = "PUT (new)"
	}
	return d
}
